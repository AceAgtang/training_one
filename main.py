# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# if openpyexl is in red hover your pointer and select install package
from email_helper import send_mail
import openpyxl


def main():
    agents_email = []
    sups_email = []
    #will open the work book by name using openpyxl library or package
    wb_obj = openpyxl.load_workbook("tarRpt_20210812_1319.xlsx")
    sheet_obj = wb_obj.active
    m_row = sheet_obj.max_row

    # loop that iterates from row 1 to the last row
    # i will always begin at 0 and will add 1 for each iteration of the loop
    for i in range(1, m_row):
        # first get the status name for each row
        status_name = sheet_obj.cell(row=i + 1, column=10).value
        # check if the status name is either  'New' or 'Edited'
        if status_name in ['New', 'Edited']:
            # if status name is either  'New' or 'Edited' code will continue here
            # store the agent name and suppervisor name and append @harthanks.com at the end
            agent = f"""{sheet_obj.cell(row=i + 1, column=5).value}@hartehanks.com"""
            supervisor = f"""{sheet_obj.cell(row=i + 1, column=14).value}@hartehanks.com"""
            # replace spaces with dot to build an assumed email of the agent and supervisor
            agent = agent.replace(" ", ".")
            supervisor = supervisor.replace(" ", ".")

            ##### TASK ######
            # 1. store the assumed agent emails to agents_email variable
            # 2. store the assumed supervisor email to sups_email
            # 3. there should be no duplicated emails in each variable
            # note: all code should be inside this if statement


    # This code below will print sups and agents email to check if you did it right
    print("sups_email : ", sups_email)
    print("agents_email : ", agents_email)

    return
    # Note:  1. remove or delete the 'return' function above if sups_email and agents_email have no duplicates
    #       2. uncomment and use the function send_mail() below to send an email substitute the variables and remove{}
    #             use your email as source email, use agents_email as destination email, use sups_email as Cc, put any
    #             string on the subject, put any message for the email body
    # Warning! the function bellow will really send an email to the recipients
    # send_fancy({source email}, {destination emails}, { Cc }, {subject of the email}, {email body or msg})

# Press the green button in the gutter to run the script.
if __name__ == '__main__':
    main()

# See PyCharm help at https://www.jetbrains.com/help/pycharm/
