import os
import smtplib
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
import csv
from tabulate import tabulate
import pandas as pd

COMMASPACE = ', '


def send_mail(send_from, send_to, cc, subject, text,
              server="smtp.office365.com"):
    assert isinstance(send_to, list)

    msg = MIMEMultipart()
    msg['From'] = send_from
    msg['To'] = COMMASPACE.join(send_to)
    msg['Date'] = formatdate(localtime=True)
    msg['Subject'] = subject

    msg.attach(MIMEText(text))

    smtp = smtplib.SMTP(server, 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(send_from, 'hartehanks-1')
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()

def send_fancy(send_from, send_to, cc,
              server="smtp.office365.com"):
    assert isinstance(send_to, list)

    msg = MIMEMultipart("alternative")


    text = """
    Supervisors,

    Below are current Open TARs to be processed. Please take a moment to review TARs.

    {table}

    """


    html = """
    <html><body><p>Supervisors,</p>
    <p> Below are current Open TARs to be processed. Please take a moment to review TARs.</p>
    {table}
    </body></html>
    """
    data = pd.read_excel("tarRpt.xlsx")
    data = data[data["StatusName"].isin(["New", "Edited"])]
    data.to_csv("tarRpt.csv", index=None, header=True)

    with open('tarRpt.csv') as input_file:
        reader = csv.reader(input_file)
        data = list(reader)

    text = text.format(table=tabulate(data, headers="firstrow", tablefmt="grid"))
    html = html.format(table=tabulate(data, headers="firstrow", tablefmt="html"))


    msg = MIMEMultipart(
        "alternative", None, [MIMEText(text), MIMEText(html, 'html')])
    msg['Subject'] = "Open TARs"
    msg['To'] = COMMASPACE.join(send_to)
    msg['Cc'] = COMMASPACE.join(cc)

    recipients = send_to + cc
    smtp = smtplib.SMTP(server, 587)
    smtp.ehlo()
    smtp.starttls()
    smtp.ehlo()
    smtp.login(send_from, 'hartehanks-1')
    smtp.sendmail(send_from, send_to, msg.as_string())
    smtp.close()
